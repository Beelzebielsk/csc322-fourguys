// Electron modules.
const ipc = require('electron').ipcRenderer;

// NPM modules.
const jsonQuery = require('json-query');

// Names of classes used for display functions
const hideClass = "hide";
const selectedClass = "selected";

// Things of class collapsible will not initially be displayed.
// When the showCollapsibleClass is added, the element will have it's display
// changed to something else; maybe inherit from something less specific.
// The visibility will be transitioned in a standard way, defined by CSS rules
// applied to all elements of class collapsible-is-shown.
const collapsibleClass = "collapsible";
const showCollapsibleClass = "collapsible-is-shown";

// Some HTML elements in the page template.
let menuArea = document.getElementById('menuArea');
let optionList = document.getElementById('optionList');

// Fetching local data.
let users;
let menus;
let menuItems;
let localData;
try {
	users = require('../assets/data/users.json');
	menus = require('../assets/data/menus.json');
	menuItems = require('../assets/data/menu-items.json');
	// For use with jsonQuery.
	localData = {
			users     : users,
			menus     : menus,
			menuItems : menuItems,
		}
} catch (e) {
	menuArea.innerHTML = "Coming Soon!";
}

// Get users who are chefs.
let chefs = jsonQuery('users[*type=chef].name', { data : localData } ).value  

// TODO: Consider changing this interface to adding a class for showing and
// removing it for hiding.

/*
function showElement (element) {
	element.classList.remove(hideClass);
}
function hideElement (element) {
	element.classList.add(hideClass);
}
function toggleVisibility (element) {
	element.classList.toggle(hideClass);
}
*/

function showElement (element) {
	element.classList.add(showCollapsibleClass);
}
function hideElement (element) {
	element.classList.remove(showCollapsibleClass);
}
function toggleVisibility (element) {
	element.classList.toggle(showCollapsibleClass);
}

// chefName: String
// returns: a string representing a button which will eventually cause a drop
// down list to appear. The containing div is for identifying the object, while
// the two objects inside of it allow for easy access to the elements within
// through css selectors.
function chefButton(chefName) {
	let startTag = "<button type=\"button\">";
	let attributes= `class="chefButton" chefName="${chefName}"`
	let endTag = "</button>";
	let body = chefName;
	return `<div ${attributes}> ${startTag} ${body} ${endTag} </div>`;
}

// Creates an HTML representation of a menu object.
// menuObj: A JSON object representing a menu
// returns: A string representing an HTML button object
// Clicking this button causes a list to appear in it's next sibling div.
function menuButton(menuObj) {
	let attributes = 'type="button" class=chefMenuButton'
  let startTag = `<button ${attributes}>`;
	let endTag = "</button>";
	let body = menuObj.name;
	return `${startTag} ${body} ${endTag}`;
}

// TODO: Change this function to use chef IDs instead of names.
// Include the IDs within elements that should reference them.
 
// Get all menus for this chef.
function getMenusFor(chefName) {
	return jsonQuery(`menus[*chef=${chefName}]`, { data : localData } ).value
}

// Create chefButtons for each chef.
for (let chef of chefs) {
	optionList.insertAdjacentHTML('beforeend', chefButton(chef));
}

// Functionality of these buttons.
// 1. Fill the optionList with chefs. Each chef is a button.
// 2. When each chef button is clicked once, expand a list of their menus
//		underneath the button.
// 3. When a menu is pressed, fill the menuArea with the food items of that menu.
//		Write a function to transform information about food into a list item.

// Give each chefButton an onclick function that will reveal a list of their
// menus, then make the list of menus disappear if clicked again.
for (let but of document.querySelectorAll('#optionList > .chefButton') ) {
	but.querySelector('button').addEventListener('click', function () {
		let chefButton = this.parentNode; // Containing Div.
		let menuList = chefButton.querySelector('.menuList');
		let button = chefButton.querySelector('button');

		// If list doesn't exist yet, create it.
		if (!menuList) {
			chefButton.insertAdjacentHTML('beforeend', 
				`<div class="menuList ${collapsibleClass}"></div>`);
			menuList = chefButton.querySelector('.menuList');
			let chefName = chefButton.attributes.chefName.value;
			let menus = getMenusFor(chefName).map( menu => menuButton(menu) ).join(" ");
			menuList.insertAdjacentHTML('beforeend', menus);
			chefMenuButtonClickEvent(menuList.querySelectorAll('.chefMenuButton'));
		}

		button.classList.toggle(selectedClass);
		toggleVisibility(menuList);
	} )
}

// Give each menuButton the ability to spawn a list of menu items to browse in
// the menuArea.
function chefMenuButtonClickEvent(buttons) {
	for (let but of buttons) {
		but.addEventListener('click', function () {
			let menuName = this.innerHTML.trim();
			let items = jsonQuery(`menuItems[*menu=${menuName}]`, { data : localData } ).value;
			if (items.length === 0) {
				menuArea.innerHTML = "No items yet. Check back later.";
				return;
			}
			let list = items.map( item => `<li> ${item.name} </li>` ).join(" ");
			menuArea.innerHTML = `<ul> ${list} </ul>`;
		} )
	}
}

// Write functionality for a general drop-down list.  
// -  It should have an expression to evaluate for it's drop down list. 
// -  It runs it on the first time it's pressed. It should also toggle
//		visibility when it's pressed.
